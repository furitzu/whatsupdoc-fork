# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(email: 'admin@example.com', password: 'password')

Doctor.create(first_name: 'Biana' ,last_name: 'Volfinzon', email: 'biana@gmail.com ', in_network_insurance: 'Maxicare', city: 'Bandung', education: 'American Board of Internal Medicine', avatar_file_name: 'doctor-image-1.jpg', avatar_content_type: 'image/jpeg', avatar_file_size:'108344', status:'1', avatar_updated_at:'2016-07-04 06:34:22.744586', specialty_id:'1',name:'Dr. Biana Volfinzon')
Doctor.create(first_name: 'Vina' ,last_name: 'Coska', email: 'vina@gmail.com ', in_network_insurance: 'Intellicare', city: 'Denpasar', education: 'American Board of Internal Medicine', avatar_file_name: 'doctor-image-2.jpg', avatar_content_type: 'image/jpeg', avatar_file_size:'108344', status:'1', avatar_updated_at:'2016-07-04 06:34:22.744586', specialty_id:'2',name:'Dr. Vina Cosko')
Doctor.create(first_name: 'A' ,last_name: 'Volfinzon', email: 'biana@gmail.com ', in_network_insurance: 'Maxicare', city: 'Bandung', education: 'American Board of Internal Medicine', avatar_file_name: 'doctor-image-1.jpg', avatar_content_type: 'image/jpeg', avatar_file_size:'108344', status:'1', avatar_updated_at:'2016-07-04 06:34:22.744586', specialty_id:'1',name:'Dr. A ')
Doctor.create(first_name: 'B' ,last_name: 'Coska', email: 'vina@gmail.com ', in_network_insurance: 'Intellicare', city: 'Denpasar', education: 'American Board of Internal Medicine', avatar_file_name: 'doctor-image-2.jpg', avatar_content_type: 'image/jpeg', avatar_file_size:'108344', status:'1', avatar_updated_at:'2016-07-04 06:34:22.744586', specialty_id:'2',name:'Dr. B')


Specialty.create(name: 'Primary Doctor', description: 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.')
Specialty.create(name: 'OB Gyne', description: 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.')	
Specialty.create(name: 'Dentist', description: 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.')	
Specialty.create(name: 'ENT', description: 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.')	
Specialty.create(name: 'Pychiatrist', description: 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.')	


Schedule.create(doctor_id: '1', name: 'Temporary Schedule', valid_from_date:'2016-07-11 14:50:00', valid_to_date: '2016-07-21 14:36:00', time_slot_id: '1')
Schedule.create(doctor_id: '2', name: 'Temporary Schedule', valid_from_date:'2016-07-11 14:50:00', valid_to_date: '2016-07-21 14:36:00', time_slot_id: '1')
Schedule.create(doctor_id: '3', name: 'Temporary Schedule', valid_from_date:'2016-07-11 14:50:00', valid_to_date: '2016-07-21 14:36:00', time_slot_id: '1')
Schedule.create(doctor_id: '4', name: 'Monday - Wednesday', valid_from_date:'2016-07-11 14:50:00', valid_to_date: '2016-07-21 14:36:00', time_slot_id: '1')

TimeSlot.create(name:'Tuesday', day:'Tuesday', start_time:' 05:00:00 ', end_time: '07:00:00', time_off:'10 Min')
