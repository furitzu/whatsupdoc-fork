class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.string :insurance 
      t.string :reason_for_visit
      t.integer :time_slot_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
