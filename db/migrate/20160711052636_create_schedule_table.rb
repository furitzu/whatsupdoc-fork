class CreateScheduleTable < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.integer :doctor_id
      t.string :name
      t.datetime :valid_from_date
      t.datetime :valid_to_date
      t.integer :time_slot_id

      t.timestamps null: false
    end
  end
end
