class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|

      t.string :first_name
      t.string :last_name
      t.string :city
      t.string :in_network_insurance
      t.string :specialty
      t.string :hospital_affiliations

      t.timestamps null: false
    end
  end
end
