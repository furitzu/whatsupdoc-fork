class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

   before_filter :configure_permitted_parameters, if: :devise_controller?
  
  # 
  def after_sign_in_path_for(resource)

	  if resource.role == "doctor"
	    doctors_path 
	  elsif resource.role == "admin"
	    rails_admin.dashboard_path 
	  elsif resource.role == "guest"
	    root_path 
	  end
	  
  end

  def configure_permitted_parameters
  	devise_parameter_sanitizer.for(:account_update){|u| u.permit(:first_name, :last_name, :date_of_birth, :gender, :mobile_number, :city, :state, :zipcode, :email)}
  	
  	devise_parameter_sanitizer.for(:sign_up){|u| u.permit(:first_name, :last_name, :date_of_birth, :gender, :mobile_number, :city, :state, :zipcode, :email, :password, :password_confirmation, :address, :role)}
  end

  #Conflict with rails_admin

  #helper method to access whoever logged in at the moment
  #private

  #def current_user
   # @current_user ||= User.find(session[:user_id]) if (session[:user_id])
  #end

  #helper_method :current_user

end
