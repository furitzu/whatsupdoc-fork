class DoctorsController < ApplicationController

before_action :find_doctor, only: [:show, :edit, :update, :destroy]

def index

	@doctors = current_user.doctors

	@appointments = Appointment.paginate(:per_page => 8, :page => params[:page])

end

def new 
	@doctor = Doctor.new
end

def create 
	@doctor = Doctor.new(doctor_params)

		if @doctor.save 
			redirect_to root_path

		else
			render 'new'
		end
end

def edit

end


def show
	
end


def update
	
	if @doctor.update(doctor_params)
		redirect_to doctor_path(@doctor)
	else
		render 'index'
	end
end

def destroy
	@doctor.destroy
	redirect_to root_path
end

private

def doctor_params
	params.require(:doctor).permit(:first_name, :last_name, :specialty, :email, :mobile_number, :zipcode, :education, :hospital_affiliations, :language_spoken, :in_network_insurance, :certification, :proffesional_membership, :proffesional_statement, :status, :license_number, :category_id, :avatar, :city,)
end

def find_doctor
	@doctor = Doctor.find(params[:id])
end

end

