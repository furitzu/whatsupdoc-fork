class SearchesController < ApplicationController

def new
	@search = Search.new
	@in_network_insurance = Doctor.uniq.pluck(:in_network_insurance)
	@city = Doctor.uniq.pluck(:city)
end

def create
	@search = Search.create(search_params)
	redirect_to @search
end

def show
	@search = Search.find(params[:id])

end

private

def search_params
	params.require(:search).permit(:specialty, :in_network_insurance, :city, :first_name, :last_name)
end

end
