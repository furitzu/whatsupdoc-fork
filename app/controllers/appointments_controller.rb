class AppointmentsController < ApplicationController
	
	before_action :find_appointment, only: [:show]

	def new
		@appointment = Appointment.new
	end

	def create
		@appointment = Appointment.new(appointment_params)

		if @appointment.save
			redirect_to root_path
		else
			render 'new'
		end	
	end

	private

	def appointment_params
		params.require(:appointment).permit(:insurance, :reason_for_visit, :new_patient, :profession_id, :time_slot_id, :user_id)
	end

	def find_appointment
		@appointment = Appointment.find(params[:id])
	end

end
