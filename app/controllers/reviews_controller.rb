class ReviewsController < ApplicationController
	before_action :find_doctor

	def new
		@review = Review.new
	end

	def create
		@review = Review.new(reviews_params)
		@review.doctor_id = @doctor.id
		@review.user_id = current_user.id


		if @review.save
			redirect_to doctor_path(@doctor)
		else
			render 'new'
		end
	end

	private

	def reviews_params
		params.require(:review).permit(:rating, :comment)
	end

	def find_doctor
		@doctor = Doctor.find(params[:doctor_id])
	end

end
