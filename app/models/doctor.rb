class Doctor < ActiveRecord::Base

belongs_to :user 
has_many :reviews
belongs_to :schedule
belongs_to :specialty

has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png" 
validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

	RailsAdmin.config do |config|
		config.model 'Doctor' do

			# Create fields
			create do 
				field :avatar
				field :name
				field :first_name
				field :last_name
				field :email
				field :mobile_number
				field :zipcode
				field :education
				field :hospital_affiliations
				field :language_spoken
				field :certification
				field :proffesional_membership
				field :in_network_insurance
				field :license_number
				field :city
				field :status
			end

			# Edit fields
			edit do 
				field :avatar
				field :name
				field :first_name
				field :last_name
				field :email
				field :mobile_number
				field :zipcode
				field :education
				field :hospital_affiliations
				field :language_spoken
				field :certification
				field :proffesional_membership
				field :in_network_insurance
				field :license_number
				field :city
				field :status
			end

			# Show fields
			list do 
				field :avatar
				field :name
				field :first_name
				field :last_name
				field :email
				field :mobile_number
				field :zipcode
				field :education
				field :hospital_affiliations
				field :language_spoken
				field :certification
				field :proffesional_membership
				field :in_network_insurance
				field :license_number
				field :city
				
			end
		end
	end

end
