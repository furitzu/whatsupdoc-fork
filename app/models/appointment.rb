class Appointment < ActiveRecord::Base

	RailsAdmin.config do |config|
		config.model 'Appointment' do
			navigation_icon 'fa fa-calendar-o'
		end
	end
end
