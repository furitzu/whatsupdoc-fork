class Schedule < ActiveRecord::Base
	has_many :doctors
	belongs_to :time_slot

	RailsAdmin.config do |config|
		config.model 'Schedule' do
			create do 
				field :doctor do
					required true
				end
				field :name
				field :valid_from_date
				field :valid_to_date
				field :time_slot do
					required true
					inline_edit false
					inline_add false
				end
			end
		end
	end

end
