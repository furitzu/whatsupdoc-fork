class TimeSlot < ActiveRecord::Base
has_many :schedules

	RailsAdmin.config do |config|
		config.model 'TimeSlot' do
			navigation_icon 'fa fa-clock-o'

			create do 
				field :name
				field :day, :enum do 
					enum do
						['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
					end
				end
				field :start_time
				field :end_time
				field :time_off, :enum do 
					enum do 
						['10 Min','15 Min','30 Min','35 Min','40 Min','45 Min','50 Min','55 Min', '60 Min (Hour)', '1.5 Hour', '2 Hour']
					end
				end
				
			end

			list do
				field :name
				field :day
				field :start_time
				field :end_time
				field :time_off
			end

			edit do 
				field :name
				field :day, :enum do 
					enum do
						['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
					end
				end
				field :start_time
				field :end_time
				field :time_off, :enum do 
					enum do 
						['10 Min','15 Min','30 Min','35 Min','40 Min','45 Min','50 Min','55 Min', '60 Min (Hour)', '1.5 Hour', '2 Hour']
					end
				end
			end

		end
	end
end
