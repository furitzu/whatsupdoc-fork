class Specialty < ActiveRecord::Base
	has_many :doctors

	RailsAdmin.config do |config|
		config.model 'Specialty' do
			
			create do
				field :name
				field :description
			end
		end
	end
end
